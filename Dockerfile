FROM docker.io/liyehaha/py3-venv:latest

RUN rpm --import http://li.nux.ro/download/nux/RPM-GPG-KEY-nux.ro
RUN rpm -Uvh http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm
RUN yum -y install ffmpeg-devel ffmpeg

ENV PROJECT=prod
ENV LANG en_US.UTF-8

ADD ./supervisord.conf /etc/supervisord.conf

RUN mkdir -p /home/works/tornado_template/

ADD . /home/works/tornado_template/
RUN /home/works/python3/bin/pip3.6 install -r /home/works/tornado_template/requirements.txt

RUN mkdir -p /home/works/supervisor/logs

EXPOSE 9993

RUN chown works.works -R /home/works/
RUN yum clean all

CMD ["supervisord", "-c", "/etc/supervisord.conf"]