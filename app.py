# -*- coding: UTF-8 -*-
import os
import tornado.httpserver
import tornado.ioloop
import tornado.web
from common import wulai_logger as logger
from common.utils import FLAGS


def load_handlers(name: str) -> list:

    """
    Load the (URL pattern, handler) tuples for each component.

    :param name: handlers dir
    :type name: str
    :return: URL pattern, handler
    :rtype: list
    """
    name = 'module.' + name
    mod = __import__(name, fromlist=['default_handlers'])
    return mod.default_handlers


class Application(tornado.web.Application):
    def __init__(self):
        handlers = []

        handlers.extend(load_handlers('webhook.handlers'))
        handlers.extend(load_handlers('send.handlers'))
        # set the URL that will be redirected from `/`
        handlers.append(
            (r'/?', tornado.web.RedirectHandler, {
                'url': 'static/a/main.html'
            })
        )

        settings = dict(
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            debug=True
        )
        tornado.web.Application.__init__(self, handlers=handlers, **settings)


if __name__ == "__main__":
    application = Application()
    http_server = tornado.httpserver.HTTPServer(application)
    port = FLAGS.port
    http_server.listen(port)
    print("Web run at local port : {} ".format(port))
    logger.info("Web run at local port %s" % (port))
    tornado.ioloop.IOLoop.instance().start()

# Sept-12-2018  QiChang.Yin         created.
