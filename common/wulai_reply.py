# -*- coding: UTF-8 -*-
from common import wulai_logger as logger
import json


# from utils import CONFIG, _redis_inst


def get_reply(params):
    logger.info("get_reply start "+">"*50)
    # 获取基本信息
    user_id=params["user_id"]
    query=params["query"]
    for one_response in params["responses"]["suggested_response"]:
        try:
            source=one_response["source"]
        except:
            source = "DEFAULT_ANSWER_SOURCE"
            logger.info("无法获取任何回复，将进行兜底回复")
        if source == "DEFAULT_ANSWER_SOURCE":
            logger.info("无法召回任何机器人和知识点，将进行兜底回复")
        elif source == "TASK_BOT":
            # 是机器人
            logger.info("召回任务机器人")
            logger.info("get_reply 传入数据：params=%s" % json.dumps(params))
            
            # 获取实体列表引用和action的值
            entities=one_response["detail"]["task"]["entities"]
            action=one_response["detail"]["task"]["action"]

            if action == "一个需要特殊处理的别名":
                # TODO
                pass
            
            if action == "另外一个需要特殊处理的别名，必须你可能需要访问数据库？读取redis？，你看着办":
                # TODO
                pass
                

        else:
            # 召回问答机器人或者关键词机器人
            logger.info("召回问答机器人或者关键词机器人，将不做处理")

    logger.info("get_reply end "+"<"*50)

    return params["responses"]["suggested_response"]
