##############################################################
# Copyright © 2018 BeiJink LaiYe Ltd. All rights reserved.
# Instruction: Send Message dataVI
# Author: QiChang.Yin
##############################################################

import json
from common import wulai_reply, wulai_logger as logger


class SendHandler():  # web.RequestHandler
    def get(self, *args, **kwargs):
        self.write("It works")
    def post(self):
        logger.info('<<<<<<<<<<<<<<<<<<<<<< in WebhookResponseHandler <<<<<<<<<<<<<<<<<<<<<<')
        params = json.loads(self.request.body)
        logger.info('webhook input params=%s' % json.dumps(params))
        query = params['msg_body']['text']['content']
        user_id = params['user_id']
        group_user_id = 0
        if 'group_info' in params and 'from_group_user' in params['group_info']:
            group_user_id = params['group_info']['from_group_user']
        responses = params['bot_response']
        response_list = []
        rsp = dict()
        rsp['suggested_response'] = response_list
        try:
            params = dict()
            params['query'] = query
            params['user_id'] = user_id
            params['group_user_id'] = group_user_id
            params['responses'] = responses
            response_list = wulai_reply.get_reply(params)
            rsp['suggested_response'] = response_list
            logger.info('response=%s' % json.dumps(rsp))
            self.set_header('Access-Control-Allow-Origin', '*')
            self.set_header('Content-Type', 'application/json; charset=UTF-8')
            self.finish(json.dumps(rsp))
        except Exception as e:
            logger.error('get_response error errmsg=%s' % (e), exc_info=True)
            self.set_header('Access-Control-Allow-Origin', '*')
            self.set_header('Content-Type', 'application/json; charset=UTF-8')
            self.write(rsp)
    logger.info('>>>>>>>>>>>>>>>>>>>>>>>> leave WebhookResponseHandler >>>>>>>>>>>>>>>>>>>>>>>>')


# -----------------------------------------------------------------------------
# URL to handler mappings
# -----------------------------------------------------------------------------
# path matches any number of `/foo[/bar...]` or just `/` or ''
path_regex = r"(?P<path>(?:(?:/[^/]+)+|/?))"

default_handlers = [
    (r"/v1/v1/send%s" % path_regex, SendHandler),
    (r"/v1/v1/send", SendHandler),
]

# Sept-12-2018  QiChang.Yin         created.

